package com.example.android.camera2basic;

import android.content.Intent;
import android.graphics.ImageFormat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.btn_yuv420).setOnClickListener(this);
        findViewById(R.id.btn_rgba8888).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(MainActivity.this, CameraActivity.class);
        switch (v.getId()) {
            case R.id.btn_yuv420:
                intent.putExtra("format",  ImageFormat.YUV_420_888);
                break;
            case R.id.btn_rgba8888:
                intent.putExtra("format",  ImageFormat.JPEG);
                break;
        }
        startActivity(intent);
    }
}
